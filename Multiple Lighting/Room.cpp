#include "Room.h"
#include <glm\gtc\matrix_transform.hpp>

using namespace std;
using namespace glm;

Room::Room(glm::vec3 position, glm::vec3 size, glm::vec3 rotation) : position(position), size(size), rotation(rotation)
{
}

void Room::draw(glm::mat4 viewMat, UniformBufferObject *ubo)
{
	if (shader != nullptr)
	{
		if (ubo != nullptr)
			ubo->bind();
		shader->bindProgram();
		if (cubemap != nullptr)
			cubemap->bind();
		else if (texture != nullptr)
		{
			texture->bind();
			shader->setUniform("textured", true);
		}
		else
			shader->setUniform("textured", false);

		if (material != nullptr)
			shader->setMaterial(material);

		mat4 modelMat(1.0f);
		modelMat = translate(modelMat, position);
		modelMat = scale(modelMat, size);
		modelMat = rotate(modelMat, rotation.x, vec3(1.0f, 0.0f, 0.0f));
		modelMat = rotate(modelMat, rotation.y, vec3(0.0f, 1.0f, 0.0f));
		modelMat = rotate(modelMat, rotation.z, vec3(0.0f, 0.0f, 1.0f));
		mat4 modelViewMat = viewMat * modelMat;
		mat3 normalMat = transpose(inverse(mat3(modelViewMat)));

		ubo->setUniform("modelMat", modelMat);
		ubo->setUniform("modelViewMat", modelViewMat);
		ubo->setUniform("normalMat", normalMat);

		glCullFace(GL_FRONT);
		model->drawRawData();
		glCullFace(GL_BACK);

		Cubemap::unbind();
		Texture::unbind();
		UniformBufferObject::unbind();
	}
}

void Room::bindCameraToRoom(FreeCamera *camera)
{
	vec3 cameraPos = camera->getPosition();
	vec3 newCameraPos = cameraPos;

	//Costrain x axis.
	if (cameraPos.x > position.x + size.x/2.0f)
		newCameraPos.x = position.x + size.x/2.0f;
	if (cameraPos.x < position.x - size.x/2.0f)
		newCameraPos.x = position.x - size.x / 2.0f;
	//Costrain y axis.
	if (cameraPos.y > position.y + size.y / 2.0f)
		newCameraPos.y = position.y + size.y / 2.0f;
	if (cameraPos.y < position.y - size.y / 2.0f)
		newCameraPos.y = position.y - size.y / 2.0f;
	//Costrain z axis.
	if (cameraPos.z > position.z + size.z / 2.0f)
		newCameraPos.z = position.z + size.z / 2.0f;
	if (cameraPos.z < position.z - size.z / 2.0f)
		newCameraPos.z = position.z - size.z / 2.0f;

	if (cameraPos != newCameraPos)
		camera->setPosition(newCameraPos);
}

Room::~Room()
{
}
