#include "Shader.h"
#include <fstream>
#include <iostream>
#include <glm/gtc/type_ptr.hpp>

using namespace std;
using namespace glm;

Shader::Shader(string vertex, string fragment, string name)
{
	uniformLocations = map<string, GLuint>();
	this->name = name;
	vertexID = glCreateShader(GL_VERTEX_SHADER);
	fragmentID = glCreateShader(GL_FRAGMENT_SHADER);

	const char * fragmentSource, *vertexSource;
	int vertexSourceLength = 0, fragmentSourceLength = 0;

	fragmentSource = loadFile(fragment, fragmentSourceLength);
	vertexSource = loadFile(vertex, vertexSourceLength);

	glShaderSource(vertexID, 1, &vertexSource, &vertexSourceLength);
	glShaderSource(fragmentID, 1, &fragmentSource, &fragmentSourceLength);

	if (compile())
	{
		programID = glCreateProgram();

		glAttachShader(programID, vertexID);
		glAttachShader(programID, fragmentID);

		glLinkProgram(programID);
		glUseProgram(programID);

		GLuint matricesIndex = glGetUniformBlockIndex(programID, "MatrixBlock");
		if (matricesIndex == GL_INVALID_INDEX)
			cout << "Could not get index for uniform block MatrixBlock in " << name << endl;
		glUniformBlockBinding(programID, matricesIndex, 0);

		GLuint lightsIndex = glGetUniformBlockIndex(programID, "LightBlock");
		if (lightsIndex == GL_INVALID_INDEX)
			cout << "Could not get index for uniform block LightBlock in " << name << endl;
		glUniformBlockBinding(programID, lightsIndex, 1);
	}

	delete[] fragmentSource;
	delete[] vertexSource;
}

bool Shader::compile()
{
	GLint compiled;
	bool successful = true;

	glCompileShader(vertexID);
	glGetShaderiv(vertexID, GL_COMPILE_STATUS, &compiled);
	if (!compiled)
	{
		cout << name << " vertex shader not compiled" << endl;
		printError(vertexID);
		successful = false;
	}

	glCompileShader(fragmentID);
	glGetShaderiv(fragmentID, GL_COMPILE_STATUS, &compiled);
	if (!compiled)
	{
		cout << name << " fragment shader not compiled" << endl;
		printError(fragmentID);
		successful = false;
	}

	return successful;
}

void Shader::bindProgram()
{
	glUseProgram(programID);
}

void Shader::setMaterial(Material *material)
{
	glUseProgram(programID);

	glUniform4fv(getUniformLocation("material.ambient"), 1, value_ptr(material->ambient));
	glUniform4fv(getUniformLocation("material.diffuse"), 1, value_ptr(material->diffuse));
	glUniform4fv(getUniformLocation("material.specular"), 1, value_ptr(material->specular));
	glUniform1f(getUniformLocation("material.shininess"), material->shininess);
}

void Shader::setUniform(std::string name, glm::mat4 matrix4)
{
	glUseProgram(programID);
	glUniformMatrix4fv(getUniformLocation(name), 1, GL_FALSE, value_ptr(matrix4));
}

void Shader::setUniform(std::string name, vec3 vector3)
{
	glUseProgram(programID);
	glUniform3fv(getUniformLocation(name), 1, value_ptr(vector3));
}

void Shader::setUniform(std::string name, vec4 vector4)
{
	glUseProgram(programID);
	glUniform4fv(getUniformLocation(name), 1, value_ptr(vector4));
}

void Shader::setUniform(std::string name, GLint integer)
{
	glUseProgram(programID);
	glUniform1i(getUniformLocation(name), integer);
}

void Shader::setUniform(std::string name, GLfloat floatingPoint)
{
	glUseProgram(programID);
	glUniform1f(getUniformLocation(name), floatingPoint);
}

GLuint Shader::getUniformLocation(string uniform)
{
	map<string, GLuint>::iterator iter = uniformLocations.find(uniform);
	if (iter == uniformLocations.end())
	{
		GLuint location = glGetUniformLocation(programID, uniform.c_str());
		uniformLocations[uniform] = location;
		if (location == -1)
			cout << "Uniform " << uniform << " not found in shader program " << name << endl;
		return location;
	}
	else
		return iter->second;
}

char* Shader::loadFile(string file, int &fileSize)
{
	int size;
	char * memblock;

	// file read based on example in cplusplus.com tutorial
	// ios::ate opens file at the end
	ifstream stream(file, ios::in | ios::binary | ios::ate);
	if (stream.is_open()) 
	{
		size = (int)stream.tellg(); // get location of file pointer i.e. file size
		memblock = new char[size];
		stream.seekg(0, ios::beg);
		stream.read(memblock, size);
		stream.close();
		cout << "Shader " << file << " loaded..." << endl;
	}
	else {
		cout << "Unable to open shader " << file << endl;
		// should ideally set a flag or use exception handling
		// so that calling function can decide what to do now
		return nullptr;
	}

	fileSize = size;
	return memblock;
}
void Shader::printError(const GLint id)
{
	int maxLength = 0;
	int logLength = 0;
	GLchar *logMessage;

	// Find out how long the error message is
	if (!glIsShader(id))
		glGetProgramiv(id, GL_INFO_LOG_LENGTH, &maxLength);
	else
		glGetShaderiv(id, GL_INFO_LOG_LENGTH, &maxLength);

	if (maxLength > 0) { // If message has some contents
		logMessage = new GLchar[maxLength];
		if (!glIsShader(id))
			glGetProgramInfoLog(id, maxLength, &logLength, logMessage);
		else
			glGetShaderInfoLog(id, maxLength, &logLength, logMessage);
		cout << "Shader Info Log:" << endl << logMessage << endl;
		delete[] logMessage;
	}
	// should additionally check for OpenGL errors here
}


Shader::~Shader()
{
}
