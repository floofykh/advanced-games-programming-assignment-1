#pragma once
#include <map>
#include <vector>
#include "Pointlight.h"
#include "Spotlight.h"
#include <string>

class LightManager
{
private:
	std::vector<Light*> *lights;
	UniformBufferObject *lightsUBO;
	const unsigned int MAX_LIGHTS = 30;
public:
	LightManager(UniformBufferObject *lightsUBO);
	void addLight(Light *light);
	void updateUniforms();
	void draw(glm::mat4 viewMat, UniformBufferObject *ubo);
	void update();
	void flipSwitches();
	void flipSwitches(std::string tags);
	~LightManager();
};

