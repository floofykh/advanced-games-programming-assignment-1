#include "UniformBufferObject_STD140.h"
#include <glm\gtc\type_ptr.hpp>

UniformBufferObject_STD140::UniformBufferObject_STD140()
{
	glGenBuffers(1, &index);
	bind();

	GLuint blockSize = 5 * sizeof(glm::mat4);
	glBufferData(GL_UNIFORM_BUFFER, blockSize, 0, GL_DYNAMIC_DRAW);

	glBindBufferBase(GL_UNIFORM_BUFFER, 0, index);
}

void UniformBufferObject_STD140::bind()
{
	glBindBuffer(GL_UNIFORM_BUFFER, index);
}

void UniformBufferObject_STD140::setMat(glm::mat4 mat, int type)
{
	if ((type >= MODEL && type <= PROJECTION))
	{
		glBufferSubData(GL_UNIFORM_BUFFER, type*sizeof(glm::mat4), sizeof(glm::mat4), glm::value_ptr(mat));
	}
}

void UniformBufferObject_STD140::setMat(glm::mat3 mat, int type)
{
	if (type == NORMAL)
	{
		for (int i = 0; i < 3; i++)
		{
			glBufferSubData(GL_UNIFORM_BUFFER, type*sizeof(glm::mat4) + sizeof(glm::vec4)*i, sizeof(glm::vec3), glm::value_ptr(mat[i]));
		}
	}
}

UniformBufferObject_STD140::~UniformBufferObject_STD140()
{
}
