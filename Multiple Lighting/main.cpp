// Windows specific: Uncomment the following line to open a console window for debug output
#if _DEBUG
#pragma comment(linker, "/subsystem:\"console\" /entry:\"WinMainCRTStartup\"")
#endif

#include "Skybox.h"
#include "Room.h"
#include "rt3d.h"
#include "Pointlight.h"
#include "Spotlight.h"
#include "UniformBufferObject.h"
#include "LightManager.h"
#include <glm\gtc\matrix_transform.hpp>
#include <random>
#include <SDL_ttf.h>

using namespace std;
using namespace glm;

bool running = true;
Shader *lightShader, *skyboxShader, *gouraudShader, *phongShader, *currentShader;
Texture *fabricTex, *metalTex;
Model *scene, *cube;
Skybox *skybox;
Cubemap *skyboxCubemap;
FreeCamera *camera;
Material *metalMaterial, *bunnyMaterial, *voidMaterial;
Spotlight *charLight;
LightManager *lightManager;


UniformBufferObject *matricesUBO, *lightsUBO;

GLuint screenWidth = 800, screenHeight = 600;

const float movementSpeed = 5.0f;

// Set up rendering context
SDL_Window * setupRC(SDL_GLContext &context) {
	SDL_Window * window;
    if (SDL_Init(SDL_INIT_VIDEO) < 0) // Initialize video
        rt3d::exitFatalError("Unable to initialize SDL"); 
	  
    // Request an OpenGL 3.0 context.
	
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 3);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE); 

	SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);  // double buffering on
	SDL_GL_SetAttribute(SDL_GL_ALPHA_SIZE, 8); // 8 bit alpha buffering
	SDL_GL_SetAttribute(SDL_GL_MULTISAMPLEBUFFERS, 1);
	SDL_GL_SetAttribute(SDL_GL_MULTISAMPLESAMPLES, 4); // Turn on x4 multisampling anti-aliasing (MSAA)
 
    // Create window
    window = SDL_CreateWindow("Multiple Lights Demo", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
		screenWidth, screenHeight, SDL_WINDOW_OPENGL | SDL_WINDOW_SHOWN);
	if (!window) // Check window was created OK
        rt3d::exitFatalError("Unable to create window");
 
    context = SDL_GL_CreateContext(window); // Create opengl context and attach to window
    SDL_GL_SetSwapInterval(1); // set swap buffers to sync with monitor's vertical refresh rate
	return window;
}

void init(void) 
{
	glm::mat4 projection = perspective(60.0f, (float)screenWidth / screenHeight, 1.0f, 10000.0f);

	metalMaterial = new Material(
		vec4(0.2f, 0.2f, 0.2f, 1.0f),
		vec4(0.5f, 0.5f, 0.5f, 1.0f),
		vec4(1.0f, 1.0f, 1.0f, 1.0f),
		1.0f);
	voidMaterial = new Material(
		vec4(0.0f, 0.0f, 0.0f, 1.0f),
		vec4(0.0f, 0.0f, 0.0f, 1.0f),
		vec4(0.0f, 0.0f, 0.0f, 1.0f),
		1.0f);
	bunnyMaterial = new Material(
		vec4(0.3f, 0.3f, 0.3f, 1.0f),
		vec4(0.9f, 0.9f, 0.9f, 1.0f),
		vec4(5.0f, 5.0f, 5.0f, 1.0f),
		0.9f);

	camera = new FreeCamera();
	camera->setPosition(vec3(-60.0f, 23.0f, 72.0f));

	lightShader = new Shader("light.vert", "light.frag", "Light");
	gouraudShader = new Shader("gouraud.vert", "gouraud.frag", "Gouraud");
	phongShader = new Shader("phong.vert", "phong.frag", "Phong");
	skyboxShader = new Shader("skyboxCubemap.vert", "skyboxCubemap.frag", "Skybox");

	currentShader = gouraudShader;

	fabricTex = new Texture();
	fabricTex->load("Textures/fabric.bmp");
	metalTex = new Texture();
	metalTex->load("Textures/Metal/studdedmetal.bmp");
	skyboxCubemap = new Cubemap();
	skyboxCubemap->load("Skyboxes/sky", "skyfront.bmp", "skyback.bmp", "skyright.bmp", "skyleft.bmp", "skytop.bmp", "skybottom.bmp");


	cube = new Model();
	cube->load("Models/cube.obj");
	cube->setMaterial(metalMaterial);
	cube->setTexture(metalTex);
	cube->setShader(gouraudShader);
	cube->setScale(vec3(2.0f, 2.0f, 2.0f));
	cube->setPosition(vec3(5.0f, 3.0f, -5.0f));

	scene = new Model();
	cout << "This may take a while..." << endl;
	scene->load("Models/spaghettiMonster.obj");
	scene->setMaterial(bunnyMaterial);
	scene->setShader(gouraudShader);
	scene->setScale(vec3(10.0f, 10.0f, 10.0f));
	scene->setPosition(vec3(-5.0f, -10.0f, -25.0f));

	skybox = new Skybox();
	skybox->setCube(cube);
	skybox->load(skyboxCubemap);
	skybox->setShader(skyboxShader);

	const GLchar * matriceNames[] =
	{
		"modelMat",
		"viewMat",
		"modelViewMat",
		"normalMat",
		"projectionMat"
	};
	matricesUBO = new UniformBufferObject("MatrixBlock", matriceNames, 5, gouraudShader, 0, GL_DYNAMIC_DRAW);
	matricesUBO->setUniform("projectionMat", projection);

	const GLchar * lightsNames[] =
	{
		"numLights",
		"position",
		"direction",
		"ambient",
		"diffuse",
		"specular",
		"attenuation",
		"cosCutoffAngle"
	};
	lightsUBO = new UniformBufferObject("LightBlock", lightsNames, 8, gouraudShader, 1, GL_DYNAMIC_DRAW);
	lightsUBO->setUniform("numLights", 0);

	lightManager = new LightManager(lightsUBO);

	Pointlight *ambientLight = new Pointlight(
		vec4(0.0f, 0.0f, 0.0f, 1.0f),
		vec4(0.2f, 0.2f, 0.1f, 1.0f),
		vec4(0.0f, 0.0f, 0.0f, 1.0f),
		vec4(0.0f, 0.0f, 0.0f, 1.0f));
	lightManager->addLight(ambientLight);
	ambientLight->setTag("Ambient Light");

	//Create a default random number generator and seed it with current time
	std::default_random_engine generator(7);
	//Create two distributions for the generator, one for the colour (float between 0 and 1), and one for the velocity(flaot between -0.1 and 0.1).
	std::uniform_real_distribution<float> lightDistribution(0.0, 1.0);

	for (int row = 0; row < 8; row++)
	{
		for (int column = 0; column < 3; column++)
		{
			Pointlight *light = new Pointlight(
				vec4(row*300.0f, 30.0f, -(column-1)*250, 1.0f),
				vec4(0.0f, 0.0f, 0.0f, 1.0f),
				vec4(lightDistribution(generator), lightDistribution(generator), lightDistribution(generator), 1.0f),
				vec4(lightDistribution(generator), lightDistribution(generator), lightDistribution(generator), 1.0f));
			light->setMesh(cube, lightShader);
			lightManager->addLight(light);
			light->setTag("Pointlight");
		}
	}

	charLight = new Spotlight(
		vec4(0.0f, 2.0f, -5.0f, 1.0f),
		vec4(0.0f, 0.0f, -1.0f, 1.0f),
		vec4(0.0f, 0.0f, 0.0f, 1.0f),
		vec4(1.0f, 1.0f, 0.5f, 1.0f),
		vec4(1.0f, 1.0f, 1.0f, 1.0f),
		20.0f
		);
	charLight->setTag("Torch");
	lightManager->addLight(charLight);

	glEnable(GL_DEPTH_TEST);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA);
}

void update(float deltaTime) {
	camera->update();

	charLight->setPosition(glm::vec4(camera->getPosition(), 1.0f));

	lightManager->update();
}

void handleInput()
{
	SDL_Event sdlEvent;
	while (SDL_PollEvent(&sdlEvent)) 
	{
		if (sdlEvent.type == SDL_KEYUP)
		{
			switch (sdlEvent.key.keysym.sym)
			{
			case SDLK_p:
				lightManager->flipSwitches("Pointlight");
				break;
			case SDLK_o:
				lightManager->flipSwitches("Torch");
				break;
			case SDLK_i:
				lightManager->flipSwitches("Ambient Light");
				break;
			case SDLK_ESCAPE:
				running = false;
				break;
			}
		}
		if (sdlEvent.type == SDL_QUIT)
			running = false;
	}

	const Uint8 *keys = SDL_GetKeyboardState(NULL);
	if (keys[SDL_SCANCODE_W]) camera->moveForward(movementSpeed);
	if (keys[SDL_SCANCODE_S]) camera->moveForward(-movementSpeed);
	if (keys[SDL_SCANCODE_A]) camera->moveRight(-movementSpeed);
	if (keys[SDL_SCANCODE_D]) camera->moveRight(movementSpeed);
	if (keys[SDL_SCANCODE_R]) camera->moveUp(movementSpeed);
	if (keys[SDL_SCANCODE_F]) camera->moveUp(-movementSpeed);

	if (keys[SDL_SCANCODE_1])
		currentShader = gouraudShader;
	if (keys[SDL_SCANCODE_2])
		currentShader = phongShader;

	if (keys[SDL_SCANCODE_COMMA]) camera->rotateY(-2.0f);
	if (keys[SDL_SCANCODE_PERIOD]) camera->rotateY(2.0f);
}


void draw(SDL_Window * window) 
{
	// clear the screen
	glEnable(GL_CULL_FACE);
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	mat4 viewMat = camera->getViewMat();
	matricesUBO->setUniform("viewMat", viewMat);
	mat3 rotateOnlyMat = glm::transpose(mat3(viewMat));
	charLight->setDirection(glm::mat4(rotateOnlyMat) * vec4(0.0f, 0.0f, -1.0f, 0.0f));

	scene->setShader(currentShader);

	//skybox->draw(viewMat, matricesUBO);

	lightManager->updateUniforms();
	lightManager->draw(viewMat, matricesUBO);

	scene->draw(viewMat, matricesUBO);

    SDL_GL_SwapWindow(window); // swap buffers
}


// Program entry point - SDL manages the actual WinMain entry point for us
int main(int argc, char *argv[]) {
    SDL_Window * hWindow; // window handle
    SDL_GLContext glContext; // OpenGL context handle
    hWindow = setupRC(glContext); // Create window and render context 

	// Required on Windows *only* init GLEW to access OpenGL beyond 1.1
	glewExperimental = GL_TRUE;
	GLenum err = glewInit();
	if (GLEW_OK != err) { // glewInit failed, something is seriously wrong
		std::cout << "glewInit failed, aborting." << endl;
		exit (1);
	}
	cout << glGetString(GL_VERSION) << endl;

	init();


	while (running)	
	{	// the event loop
		handleInput();
		update(1.0f);
		draw(hWindow); // call the draw function
	}

    SDL_GL_DeleteContext(glContext);
    SDL_DestroyWindow(hWindow);
    SDL_Quit();
    return 0;
}