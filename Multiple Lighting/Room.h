#pragma once
#include "Model.h"
#include "Cubemap.h"
#include "FreeCamera.h"
#include "UniformBufferObject.h"

class Room
{
private:
	Model *model;
	Texture *texture;
	Cubemap *cubemap;
	Material *material;
	Shader *shader;
	glm::vec3 position, size, rotation;
public:
	Room(glm::vec3 position, glm::vec3 size, glm::vec3 rotation);
	void setModel(Model *model) { this->model = model; }
	void setShader(Shader *shader) { this->shader = shader; }
	void setTexture(Texture *texture) { this->texture = texture; }
	void setTexture(Cubemap *cubemap) { this->cubemap = cubemap; }
	void setMaterial(Material *material) { this->material = material; }
	void draw(glm::mat4 viewMat, UniformBufferObject *ubo);
	void bindCameraToRoom(FreeCamera *camera);
	~Room();
};

