// phong.vert
#version 330

const int maxLights = 30;

layout (std140) uniform MatrixBlock
{
	mat4 modelMat;
	mat4 viewMat;
	mat4 modelViewMat;
	mat4 projectionMat;
	mat3 normalMat;
};

layout (std140) uniform LightBlock
{
	int numLights;
	vec4 position[maxLights];
	vec4 direction[maxLights];
	vec4 ambient[maxLights];
	vec4 diffuse[maxLights];
	vec4 specular[maxLights];
	vec3 attenuation[maxLights];
	float cosCutoffAngle[maxLights];
};

layout (location = 0) in vec3 in_Position;
layout (location = 2) in vec3 in_Normal;
layout (location = 3) in vec2 in_TexCoord;

out VS_OUT
{
	vec3 vs_normalVec;
	vec3 vs_vertexVec;
	vec2 vs_texCoord;
	vec3 vs_lightVec[maxLights];
	float vs_lightVertDistance[maxLights];
	float vs_attenuation[maxLights];
} vs_out;


void calculateVectors()
{
		//Translate vertex position into view coordinates. 
	vec4 vertexPosition = modelMat * vec4(in_Position, 1.0);

		//Find vertex position in view coordinates. Since the viewer is at the origin, simply negate the position. 
	vs_out.vs_vertexVec = normalize(-vertexPosition).xyz;

		//Translate normal of the vertex to view coordinates. Uses the normal matrix. 
	vs_out.vs_normalVec = normalize(normalMat * in_Normal);

	for(int i=0; i<numLights; ++i)
	{
			//Calculate the vector from the vertex position to the light source in view coordinates. 
		vs_out.vs_lightVec[i] = position[i].xyz - vertexPosition.xyz;

			//Calculate the distance of the above vector before normalising it. 
		vs_out.vs_lightVertDistance[i] = length(vs_out.vs_lightVec[i]);
		vs_out.vs_lightVec[i] = normalize(vs_out.vs_lightVec[i]);

		vs_out.vs_attenuation[i] = 1 / 
			(attenuation[i].x + 
			attenuation[i].y*vs_out.vs_lightVertDistance[i] + 
			attenuation[i].z*vs_out.vs_lightVertDistance[i]*vs_out.vs_lightVertDistance[i]);
	}
}

void main(void) 
{
	calculateVectors();

	vs_out.vs_texCoord = in_TexCoord;

	gl_Position = projectionMat * modelViewMat * vec4(in_Position, 1.0);

}