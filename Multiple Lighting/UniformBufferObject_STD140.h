#pragma once
#include "Shader.h"

class UniformBufferObject_STD140
{
private:
	GLuint index;
public:
	UniformBufferObject_STD140();
	void bind();
	void setMat(glm::mat4 mat, int type);
	void setMat(glm::mat3 mat, int type);
	~UniformBufferObject_STD140();


	enum MATRIX_POSITION
	{
		MODEL,
		VIEW,
		MODELVIEW,
		PROJECTION,
		NORMAL
	};
};

