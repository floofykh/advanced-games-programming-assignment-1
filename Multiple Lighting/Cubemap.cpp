#include "Cubemap.h"
#include <SDL.h>
#include <iostream>

using namespace std;

Cubemap::Cubemap()
{
}

bool Cubemap::load(std::string folder, std::string back, std::string front, std::string right, std::string left, std::string up, std::string down)
{
	glGenTextures(1, &id); // generate texture ID
	GLenum sides[6] = { GL_TEXTURE_CUBE_MAP_POSITIVE_Z,
		GL_TEXTURE_CUBE_MAP_NEGATIVE_Z,
		GL_TEXTURE_CUBE_MAP_POSITIVE_X,
		GL_TEXTURE_CUBE_MAP_NEGATIVE_X,
		GL_TEXTURE_CUBE_MAP_POSITIVE_Y,
		GL_TEXTURE_CUBE_MAP_NEGATIVE_Y };

	string backS = folder + "/" + back;
	string frontS = folder + "/" + front;
	string rightS = folder + "/" + right;
	string leftS = folder + "/" + left;
	string upS = folder + "/" + up;
	string downS = folder + "/" + down;

	const char *fname[6] =
	{
		backS.c_str(),
		frontS.c_str(),
		rightS.c_str(),
		leftS.c_str(),
		upS.c_str(),
		downS.c_str()
	};

	SDL_Surface *tmpSurface;

	glBindTexture(GL_TEXTURE_CUBE_MAP, id); // bind texture and set parameters
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);

	for (int i = 0; i<6; i++)
	{
		// load file - using core SDL library
		tmpSurface = SDL_LoadBMP(fname[i]);
		if (!tmpSurface)
		{
			cout << "Error loading bitmap " << fname[i] << endl;
			return false;
		}

		glTexImage2D(sides[i], 0, GL_RGB, tmpSurface->w, tmpSurface->h, 0,
			GL_BGR, GL_UNSIGNED_BYTE, tmpSurface->pixels);
		// texture loaded, free the temporary buffer
		SDL_FreeSurface(tmpSurface);
	}

	return true;
}

void Cubemap::bind()
{
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_CUBE_MAP, id);
}

void Cubemap::bind(GLenum bindingPoint)
{
	glActiveTexture(bindingPoint);
	glBindTexture(GL_TEXTURE_CUBE_MAP, id);
}

void Cubemap::unbind()
{
	glBindTexture(GL_TEXTURE_CUBE_MAP, 0);
}

Cubemap::~Cubemap()
{
}
