#pragma once
#include <string>
#include <GL\glew.h>

class Cubemap
{
private:
	GLuint id;
public:
	Cubemap();
	bool load(std::string folder, std::string back, std::string front, std::string right, std::string left, std::string up, std::string down);
	void bind();
	void bind(GLenum bindingPoint);
	static void unbind();
	~Cubemap();
};

