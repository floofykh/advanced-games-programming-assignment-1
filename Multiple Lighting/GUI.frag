#version 330

precision highp float;

layout (std140) uniform matrices
{
	mat4 modelMat;
	mat4 viewMat;
	mat4 modelViewMat;
	mat4 projectionMat;
	mat3 normalMat;
};

uniform sampler2D sample;

layout (location = 0) out vec4 outColour;
in vec2 texCoord;
in vec3 colour;

uniform samplerCube cubeMap;
 
void main(void) 
{   
	outColour = texture(sample, texCoord);
}