//phong.frag
#version 330

// Some drivers require the following
precision highp float;

struct materialStruct
{
	vec4 ambient;
	vec4 diffuse;
	vec4 specular;
	float shininess;
};

const int maxLights = 30;

vec3 reflection;

layout (std140) uniform LightBlock
{
	int numLights;
	vec4 position[maxLights];
	vec4 direction[maxLights];
	vec4 ambient[maxLights];
	vec4 diffuse[maxLights];
	vec4 specular[maxLights];
	vec3 attenuation[maxLights];
	float cosCutoffAngle[maxLights];
};

uniform materialStruct material;
uniform sampler2D textureUnit0;
uniform bool textured = true;

in VS_OUT
{
	vec3 vs_normalVec;
	vec3 vs_vertexVec;
	vec2 vs_texCoord;
	vec3 vs_lightVec[maxLights];
	float vs_lightVertDistance[maxLights];
	float vs_attenuation[maxLights];
} fs_in;

layout (location = 0) out vec4 colour;


vec4 calculateLightColour(int i)
{
		//Calculate colour of light and object material.
		//Ambient intensity
	vec4 ambientI = ambient[i] * material.ambient;

		// Diffuse intensity
	vec4 diffuseI =  diffuse[i] * material.diffuse;
	diffuseI = diffuseI * max(dot(fs_in.vs_normalVec, fs_in.vs_lightVec[i]), 0.0) * fs_in.vs_attenuation[i];

		// Specular intensity
	vec4 specularI = specular[i] * material.specular;
		//Specular Intensity = Ks*(R.V)^(alpha)*Is
	specularI = specularI * pow( max( dot( reflection, fs_in.vs_vertexVec ), 0 ), material.shininess ) * fs_in.vs_attenuation[i];

	return ambientI + diffuseI + specularI;
}

vec4 calculateLight(int i)
{
		//Calculate reflection light makes to the surface. 
	reflection = reflect( -fs_in.vs_lightVec[i], fs_in.vs_normalVec );

	if(cosCutoffAngle[i] == 0 && direction[i] == vec4(0.0, 0.0, 0.0, 0.0))
	{
		return calculateLightColour(i);
	}
	else
	{
			//Calculate the direction vector of the light in view space. 
		vec3 spotDirection = normalize((position[i] - normalize(direction[i])).xyz);

			//Get dot product of spotlight direction and vertex vector. 
		float dotProduct = dot(fs_in.vs_lightVec[i], -normalize(direction[i].xyz));

		if(dotProduct > cosCutoffAngle[i])
		{
			return calculateLightColour(i);
		}
		else
			return vec4(0.0, 0.0, 0.0, 1.0);
	}
}
 
void main(void)
{
	vec4 lightColour = vec4(0.0, 0.0, 0.0, 1.0);

	for(int i=0; i<numLights; ++i)
	{
		lightColour += vec4(calculateLight(i).rgb, 0.0);
	}
	
	if(textured)
		colour = lightColour * texture(textureUnit0, fs_in.vs_texCoord);
	else
		colour = lightColour;
}